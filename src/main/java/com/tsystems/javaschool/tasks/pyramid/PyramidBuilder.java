package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.isEmpty()) {
            throw new CannotBuildPyramidException();
        }

        int height = pyramidHeight(inputNumbers.size());
        if (height == -1) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        int[][] result = new int[height][2 * height - 1];
        int rowLength = 1;
        int position = 0;
        for (int i = 0; i < height; i++) {
            for (int j = height - rowLength; j <= height + rowLength - 2; j += 2) {
                result[i][j] = inputNumbers.get(position++);
            }
            rowLength++;
        }

        return result;
    }

    /**
     *
     * @param n - number of elements
     * @return height of the pyramid or -1 if impossible to build a pyramid of n elements
     */
    private static int pyramidHeight(long n) {
        int height = 1;
        for (long i = 0; i <= n; i += height, height++) {
            if (i == n) {
                return height - 1;
            }
        }
        return -1;
    }


}
