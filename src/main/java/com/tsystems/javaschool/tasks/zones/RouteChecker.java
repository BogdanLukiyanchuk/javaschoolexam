package com.tsystems.javaschool.tasks.zones;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        if (requestedZoneIds.isEmpty()) {
            return true;
        }
        Map<Integer, Zone> zoneMap = zoneState.stream().collect(Collectors.toMap(Zone::getId, Function.identity()));

        Set<Integer> requestedIds = new HashSet<>(requestedZoneIds);
        if (requestedIds.stream().anyMatch(reqId -> !zoneMap.containsKey(reqId))) {
            return false;
        }

        Integer first = requestedZoneIds.get(0);
        requestedIds.remove(first);

        Set<Integer> oldIds = new HashSet<>();
        oldIds.add(first);

        while (!requestedIds.isEmpty()) {
            Set<Integer> newIds = new HashSet<>();
            for (Integer id : oldIds) {
                for (Integer newId : zoneMap.get(id).getNeighbours()) {
                    if (requestedIds.contains(newId)) {
                        requestedIds.remove(newId);
                        newIds.add(newId);
                    }
                }
            }
            for (Integer id : requestedIds) {
                for (Integer neighbourId : zoneMap.get(id).getNeighbours()) {
                    if (oldIds.contains(neighbourId)) {
                        requestedIds.remove(id);
                        newIds.add(id);
                        break;
                    }
                }
            }
            if (newIds.isEmpty()) {
                return false;
            }
            oldIds = newIds;
        }
        return true;
    }

}
